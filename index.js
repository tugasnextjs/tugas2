function MyForm() {
    const [register, setRegister] = React.useState({
        nama: "",
        phone: "",
        email: "",
        company: "",
        services: "Development",
        budget:"$5.000 - $10.000"
      });

    const [currentTab, setcurrentTab] = React.useState(0);
    
    const updateService =(opt,idx) =>{
        try{
            var elements =document.getElementsByClassName("optionTab1");
            for(let i=0;i<elements.length;i++){
                if(i==idx){
                    elements[i].style.borderColor = "#4A3AFF";
                }else{
                    elements[i].style = null;
                }
            }
            setRegister(previousState => {
                return { ...previousState, services: opt}
            });
            
            
        }catch (error) {
            console.log("Error: " + error);
        }
    }; 
    const updateBudget =(idx) =>{
        try{
            
            var elements =document.getElementsByClassName("optionTab2");
            
            var radiobtn = document.getElementsByClassName("budgetInput");
            for(let i=0;i<elements.length;i++){
                if(i==idx){
                    elements[i].style.borderColor = "#4A3AFF";
                    setRegister(previousState => {
                        return { ...previousState, budget: radiobtn[i].value}
                    });
                }else{
                    elements[i].style = null;
                    
                }
            }
            
        }catch (error) {
            console.log("Error: " + error);
        }
    }; 
    const updateNama = (answer) => {
        setRegister(previousState => {
          return { ...previousState, nama: answer}
        });
    }
    const updatePhone = (answer) => {
        setRegister(previousState => {
          return { ...previousState, phone: answer}
        });
    }
    const updateEmail = (answer) => {
        setRegister(previousState => {
          return { ...previousState, email: answer}
        });
    }
    const updateCompany = (answer) => {
        setRegister(previousState => {
          return { ...previousState, company: answer}
        });
    }

    const handleNext = (event) => {
        event.preventDefault();
        validate("next");
    };

    const handlePrev = (event) => {
        event.preventDefault();
        validate("prev");
    };
    
    const handleSubmit = (event) => {
        event.preventDefault();
        const myJSON = JSON.stringify(register);
        alert(myJSON);
    };
      
    const validate = (sec) => {
        if(sec=="next"){
            if(currentTab==0){
                let err=0;
                if (register.company.trim().length == 0) {
                    document.getElementById("inputCompany").focus();
                    document.getElementById("pesanCompany").textContent = "Company is required";
                    document.getElementById("labelCompany").style.color = "#962DFF";
                    document.getElementById("inputCompany").style.borderColor = "#962DFF";
                    err=err+1;
                }else{
                    document.getElementById("pesanCompany").textContent = "";
                    document.getElementById("labelCompany").style = null;
                    document.getElementById("inputCompany").style = null;
                }

                if (register.phone.trim().length == 0) {
                    document.getElementById("inputPhone").focus();
                    document.getElementById("pesanPhone").textContent = "Phone number is required";
                    document.getElementById("labelPhone").style.color = "#962DFF";
                    document.getElementById("inputPhone").style.borderColor = "#962DFF";
                    err=err+1;
                }else{
                    
                    if((String(register.phone)).startsWith("08") && (String(register.phone)).length>7 && (String(register.phone)).length<13){
                        document.getElementById("pesanPhone").textContent = "";
                        document.getElementById("labelPhone").style = null;
                        document.getElementById("inputPhone").style = null;
                    }else{
                        document.getElementById("inputPhone").focus();
                        document.getElementById("pesanPhone").textContent = "Phone number is invalid";
                        document.getElementById("labelPhone").style.color = "#962DFF";
                        document.getElementById("inputPhone").style.borderColor = "#962DFF";
                        err=err+1;
                    }
                }
                if (register.email.trim().length == 0) {
                    document.getElementById("inputEmail").focus();
                    document.getElementById("pesanEmail").textContent = "Email is required";
                    document.getElementById("labelEmail").style.color = "#962DFF";
                    document.getElementById("inputEmail").style.borderColor = "#962DFF";
                    err=err+1;
                }else{
                    if(register.email.endsWith("gmail.com")){
                        document.getElementById("pesanEmail").textContent = "";
                        document.getElementById("labelEmail").style = null;
                        document.getElementById("inputEmail").style = null;
                    }else{
                        document.getElementById("inputEmail").focus();
                        document.getElementById("pesanEmail").textContent = "Email is invalid";
                        document.getElementById("labelEmail").style.color = "#962DFF";
                        document.getElementById("inputEmail").style.borderColor = "#962DFF";
                        err=err+1;
                    }
                }
                if (register.nama.trim().length == 0) {
                    document.getElementById("inputName").focus();
                    document.getElementById("pesanNama").textContent = "Name is required";
                    document.getElementById("labelNama").style.color = "#962DFF";
                    document.getElementById("inputName").style.borderColor = "#962DFF";
                    err=err+1;
                }else{
                    document.getElementById("pesanNama").textContent = "";
                    document.getElementById("labelNama").style = null;
                    document.getElementById("inputName").style = null;
                }
                console.log(err);
                if(err==0){
                    document.getElementById("tab0").style.display = "none";
                    document.getElementById("tab1").style.display = "block";
                    document.getElementById("tab2").style.display = "none";
                    document.getElementById("tab3").style.display = "none";
                    document.getElementById("prevBtn").style.display = "inline";
                    setcurrentTab(currentTab+1);
                }
            }else if(currentTab==1){
                document.getElementById("tab0").style.display = "none";
                document.getElementById("tab1").style.display = "none";
                document.getElementById("tab2").style.display = "block";
                document.getElementById("tab3").style.display = "none";
                document.getElementById("prevBtn").style.display = "inline";
                setcurrentTab(currentTab+1);
                
            }else if(currentTab==2){
                document.getElementById("tab0").style.display = "none";
                document.getElementById("tab1").style.display = "none";
                document.getElementById("tab2").style.display = "none";
                document.getElementById("tab3").style.display = "block";
                document.getElementById("prevBtn").style.display = "inline";
                document.getElementById("nextBtn").style.display = "none";
                setcurrentTab(currentTab+1);
            }
        }else if(sec=="prev"){
            if (currentTab==3){
                document.getElementById("tab0").style.display = "none";
                document.getElementById("tab1").style.display = "none";
                document.getElementById("tab2").style.display = "block";
                document.getElementById("tab3").style.display = "none";
                document.getElementById("prevBtn").style.display = "inline";
                document.getElementById("nextBtn").style.display = "inline";
                
            }
            else if(currentTab==2){
                document.getElementById("tab0").style.display = "none";
                document.getElementById("tab1").style.display = "block";
                document.getElementById("tab2").style.display = "none";
                document.getElementById("tab3").style.display = "none";
                document.getElementById("prevBtn").style.display = "inline";
                document.getElementById("nextBtn").style.display = "inline";
            }else if(currentTab==1){
                document.getElementById("tab0").style.display = "block";
                document.getElementById("tab1").style.display = "none";
                document.getElementById("tab2").style.display = "none";
                document.getElementById("tab3").style.display = "none";
                document.getElementById("prevBtn").style.display = "none";
                document.getElementById("nextBtn").style.display = "inline";
            }
            setcurrentTab(currentTab-1);  
        }
        console.log("Current: "+currentTab);
    };

    
    return (
      <form id="frm">
        <h1 className="ratatengah">Get a project quote</h1>
        <p className="ratatengah">Please fill the form below to receive a quote for your project. Feel free to add as much detail as needed.</p>
        <br/>
        <div id="tab0" className="tab" style={{display:"block"}}>
            <div id="atas">
                <span id="blue">1</span>
                <progress id="colored" value="50" max="100"> 50% </progress>
                <span id="grey">2</span>
                <progress id="greybar" value="0" max="100"> 0% </progress>
                <span id="grey">3</span>
                <progress id="greybar" value="0" max="100"> 0% </progress>
                <span id="grey">4</span>
                <hr/>
            </div>
            <h3>Contact details</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisc.</p>
            <br/>

            <div className="row">
                <div className="column">
                    <label><b id="labelNama">Name</b><br/><br/>
                    <input
                        type="text" id="inputName" className="ip2"  name="nama" placeholder="Name" style={{backgroundImage: "url(asset/name.png)"}}
                        value={register.nama}
                        onChange={(e) => updateNama(e.target.value)}
                    />
                    </label>
                    <p id="pesanNama" className="pesanError"></p>    
                    <label><b id="labelPhone">Phone Number</b><br/><br/>
                    <input
                        type="number" id="inputPhone" className="ip2" name="phone" placeholder="Phone Number" style={{backgroundImage: "url(asset/phone.png)"}}
                        value={register.phone}
                        onChange={(e) => updatePhone(e.target.value)}
                    />
                    </label>
                    <p id="pesanPhone" className="pesanError"></p>
                </div>
                <div className="column">
                    <label><b id="labelEmail">Email</b><br/><br/>
                    <input
                        type="text" id="inputEmail" className="ip2" name="email" placeholder="Email" style={{backgroundImage: "url(asset/email.png)"}}
                        value={register.email}
                        onChange={(e) => updateEmail(e.target.value)}
                    />
                    </label>
                    <p id="pesanEmail" className="pesanError"></p>
                    <label><b id="labelCompany">Company</b><br/><br/>
                    <input
                        type="text" id="inputCompany" className="ip2" name="company" placeholder="Company" style={{backgroundImage: "url(asset/company.png)"}}
                        value={register.company}
                        onChange={(e) => updateCompany(e.target.value)}
                    />
                    </label>
                    <p id="pesanCompany" className="pesanError"></p>
                </div>
            </div>
        </div>
        <div id="tab1" className="tab">
            <div id="atas">
                <span id="blue">1</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">2</span>
                <progress id="colored" value="50" max="100"> 50% </progress>
                <span id="grey">3</span>
                <progress id="greybar" value="0" max="100"> 0% </progress>
                <span id="grey">4</span>
                <hr/>
            </div>
            <h3>Our services</h3>
            <p>Please select which service you are interested in.</p>
            <br/>
            <div className="row">
                <div className="column">
                    <div className="optionTab1" onClick={() => updateService("Development",0)} style={{borderColor:'#4A3AFF'}}>
                        <div id="circleTab1" style={{float:'left'}}>
                            <img id="imgTab1" src="asset/development.png"  width="50%" height="50%"/>
                        </div>
                        <div id ="textTab1"><b>Development</b></div>
                    </div>
                    <div className="optionTab1" onClick={() => updateService("Marketing",1)}>
                        <div id="circleTab1" style={{float:'left'}}>
                            <img id="imgTab1" src="asset/marketing.png"  width="50%" height="50%"/>
                        </div>
                        <div id ="textTab1"><b>Marketing</b></div>
                    </div>
                </div>
                <div className="column">
                    <div className="optionTab1" onClick={() => updateService("Web Design",2)}>
                        <div id="circleTab1" style={{float:'left'}}>
                            <img id="imgTab1" src="asset/webdesign.png"  width="50%" height="50%"/>
                        </div>
                        <div id ="textTab1"><b>Web Design</b></div>
                    </div>
                    <div className="optionTab1" onClick={() => updateService("Other",3)}>
                        <div id="circleTab1" style={{float:'left'}}>
                            <img id="imgTab1" src="asset/other.png"  width="50%" height="50%"/>
                        </div>
                        <div id ="textTab1"><b>Other</b></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab2" className="tab">
            <div id="atas">
                <span id="blue">1</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">2</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">3</span>
                <progress id="colored" value="50" max="100"> 50% </progress>
                <span id="grey">4</span>
                <hr/>
            </div>
                <h3>What’s your project budget?tes</h3>
                <p>Please select the project budget range you have in mind.</p>
                <br/>
                <div className="row">
                    <div className="column">
                        <div className="optionTab2" style={{borderColor:'#4A3AFF'}} onClick={()=>updateBudget(0)}>
                            <input className="budgetInput" type="radio" name="budget" value="$5.000 - $10.000" checked={register.budget==="$5.000 - $10.000"} onChange={e => {}}/>$5.000 - $10.000
                        </div>
                        <div className="optionTab2" onClick={()=>updateBudget(1)}>
                            <input className="budgetInput" type="radio" name="budget" value="$20.000 - $50.000" checked={register.budget==="$20.000 - $50.000"} onChange={e => {}}/>$20.000 - $50.000
                        </div>
                    </div>
                    <div className="column">
                    <   div className="optionTab2" onClick={()=>updateBudget(2)}>
                            <input className="budgetInput" type="radio" name="budget" value="$10.000 - $20.000"  checked={register.budget==="$10.000 - $20.000"} onChange={e => {}}/>$10.000 - $20.000
                        </div>
                        <div className="optionTab2" onClick={() => updateBudget(3)}>
                            <input className="budgetInput" type="radio" name="budget"  value="$50.000+"  checked={register.budget==="$50.000+"} onChange={e => {}}/>$50.000+
                        </div>
                    </div>
                </div>
        </div>
        <div id="tab3" className="tab">
            <div id="atas">
                <span id="blue">1</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">2</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">3</span>
                <progress id="colored" value="100" max="100"> 100% </progress>
                <span id="blue">4</span>
                <hr/>
            </div>
            <img id="imgSubmit" src="asset/submit.png" />
            <div id="centered">
                <h3>Submit your quote request</h3>
                <p>Please review all the information you previously typed in the past steps, and if all is okay, submit your message to receive a project quote in 24 - 48 hours.</p>
                <button id="sumbitBtn" onClick={handleSubmit}>Submit</button>
            </div>
        </div>
        <button id="prevBtn" onClick={handlePrev}>Previous step</button>
        <button id="nextBtn" onClick={handleNext}>Next step</button>
      </form>
    )
  }
  
  const root = ReactDOM.createRoot(document.getElementById('quest'));
  root.render(<MyForm />);